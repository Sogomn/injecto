const STORAGE_KEY_SCRIPTS = "scripts";

window.addEventListener("load", () => {
	const selector = document.getElementById("selector");
	const codeArea = document.getElementById("code-area");
	const styleArea = document.getElementById("style-area");
	const inputFilter = document.getElementById("input-filter");
	const buttonAdd = document.getElementById("button-add");
	const buttonDelete = document.getElementById("button-delete");
	const buttonSave = document.getElementById("button-save");
	
	browser.storage.sync.get(STORAGE_KEY_SCRIPTS).then(result => {
		const scripts = result.scripts;
		
		initScriptList(selector, scripts);
		initTextAreaListeners(selector, codeArea, styleArea);
		initControlListeners(selector, codeArea, styleArea, inputFilter, buttonAdd, buttonDelete, buttonSave);
	});
});

function initScriptList(selector, scripts) {
	scripts?.forEach(script => {
		const option = document.createElement("option");
		
		option.text = script.filter || "";
		option.value = JSON.stringify(script);
		
		selector.add(option);
	});
}

function initTextAreaListeners(selector, codeArea, styleArea) {
	selector.addEventListener("change", () => {
		const script = JSON.parse(selector.value);
		
		codeArea.value = script?.code || "";
		styleArea.value = script?.style || "";
	});
	codeArea.addEventListener("input", () => {
		if (selector.selectedIndex >= 0) {
			const option = selector.item(selector.selectedIndex);
			const script = JSON.parse(option.value);
			
			script.code = codeArea.value;
			
			option.value = JSON.stringify(script);
		}
	});
	styleArea.addEventListener("input", () => {
		if (selector.selectedIndex >= 0) {
			const option = selector.item(selector.selectedIndex);
			const script = JSON.parse(option.value);
			
			script.style = styleArea.value;
			
			option.value = JSON.stringify(script);
		}
	});
	
	window.onkeydown = e => {
		if (e.key == "Tab") {
			e.preventDefault();
			
			if (document.activeElement == codeArea) {
				codeArea.setRangeText("\t", codeArea.selectionStart, codeArea.selectionEnd, "end");
			} else if (document.activeElement == styleArea) {
				styleArea.setRangeText("\t", styleArea.selectionStart, styleArea.selectionEnd, "end");
			}
		}
	};
}

function initControlListeners(selector, codeArea, styleArea, inputFilter, buttonAdd, buttonDelete, buttonSave) {
	const addScript = () => {
		if (!inputFilter.value) {
			return;
		}
		
		const option = document.createElement("option");
		
		option.text = inputFilter.value;
		option.value = JSON.stringify({
			filter: inputFilter.value,
			code: "",
			style: "",
		});
		inputFilter.value = "";
		
		selector.add(option);
	};
	
	buttonAdd.addEventListener("click", addScript);
	buttonDelete.addEventListener("click", () => {
		const selectedIndex = selector.selectedIndex;
		
		selector.remove(selectedIndex);
		
		selector.selectedIndex = -1;
		codeArea.value = "";
		styleArea.value = "";
	});
	buttonSave.addEventListener("click", () => {
		const scripts = [];
		
		for (let i = 0; i < selector.length; i++) {
			const option = selector.item(i);
			const script = JSON.parse(option.value);
			
			scripts.push({
				filter: option.text,
				code: script.code,
				style: script.style,
			});
		}
		
		browser.storage.sync.set({
			[STORAGE_KEY_SCRIPTS]: scripts,
		}).then(() => {
			browser.runtime.sendMessage({});
		});
	});
}
