const STORAGE_KEY_SCRIPTS = "scripts";
const SCRIPT_HANDLES = [];

browser.browserAction.onClicked.addListener(() => {
	browser.runtime.openOptionsPage();
});

browser.runtime.onInstalled.addListener(reloadScripts);
browser.runtime.onStartup.addListener(reloadScripts);
browser.runtime.onMessage.addListener(reloadScripts);

function loadScripts() {
	browser.storage.sync.get(STORAGE_KEY_SCRIPTS).then(result => {
		result.scripts?.forEach(script => {
			const contentScript = {
				matches: [script.filter],
				js: [{code: script.code}],
				css: [{code: script.style}],
				runAt: "document_start",
			};
			const handle = browser.contentScripts.register(contentScript);
			
			SCRIPT_HANDLES.push(handle);
		});
	});
}

function reloadScripts() {
	Promise.all(SCRIPT_HANDLES).then(contentScripts => {
		contentScripts.forEach(contentScript => {
			contentScript.unregister();
		});
		
		SCRIPT_HANDLES.length = 0;
		
		loadScripts();
	});
}
